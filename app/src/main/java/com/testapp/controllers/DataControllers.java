package com.testapp.controllers;

import com.testapp.pojoobjects.Article;
import java.util.ArrayList;

public class DataControllers {


    private static DataControllers ourInstance = new DataControllers();

    private ArrayList<Article> newsList;

    public static DataControllers getInstance() {
        return ourInstance;
    }

    private DataControllers() {
    }


    public ArrayList<Article> getNewsList() {
        return newsList;
    }

    public void setNewsList(ArrayList<Article> newsList) {
        this.newsList = newsList;
    }

}
