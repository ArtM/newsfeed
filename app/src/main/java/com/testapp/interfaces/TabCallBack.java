package com.testapp.interfaces;

public interface TabCallBack {
    void tabCallBack(int position, boolean checked);
}
