package com.testapp.interfaces;

public interface SavedCallBack {
    void saveItem(int position, boolean checked);
}
