package com.testapp.interfaces;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(View item, int position);
}
