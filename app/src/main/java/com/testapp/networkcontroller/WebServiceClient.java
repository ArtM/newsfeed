package com.testapp.networkcontroller;

import android.content.Context;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class WebServiceClient {

    public static void getNews(Context context, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        NewsFeedHttpClient.get(context, "/v2/everything", params, responseHandler);
    }

}
