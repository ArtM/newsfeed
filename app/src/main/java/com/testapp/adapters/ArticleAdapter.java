package com.testapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.testapp.R;
import com.testapp.interfaces.OnItemClickListener;
import com.testapp.pojoobjects.Article;

import java.util.ArrayList;

/*
 * Article Adapter for an Articles list
 */
public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.ViewHolder> {

    private OnItemClickListener onItemClickListener;
    private Context context;
    private ArrayList<Article> newsList;

    public ArticleAdapter(Context context) {
        this.context = context;
        this.newsList = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_news_feed, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView title = holder.title;
        TextView description = holder.description;
        TextView content = holder.content;
        ImageView feedImg = holder.feedImg;

        //Set item texts
        title.setText(newsList.get(position).getTitle());
        description.setText(newsList.get(position).getDescription());
        content.setText(newsList.get(position).getContent());

        //Load and show an image of the article
        Glide.with(context).load(newsList.get(position).getUrlToImage()).into(feedImg);
    }


    /*
     * Get the Articles list count
     */
    @Override
    public int getItemCount() {
        return newsList == null ? 0 : newsList.size();
    }

    public void addNewsList(ArrayList<Article> data) {
        newsList.clear();
        newsList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView description;
        TextView content;
        TextView title;

        ImageView feedImg;

        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            content = itemView.findViewById(R.id.content);

            feedImg = itemView.findViewById(R.id.feedImg);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(v, getAdapterPosition());
                }
            });

        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}
