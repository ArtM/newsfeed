package com.testapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.testapp.R;
import com.testapp.controllers.DataControllers;
import com.testapp.interfaces.OnItemClickListener;
import com.testapp.pojoobjects.Article;

import java.util.ArrayList;

/*
 * Adapter for saved newsFeed
 */
public class SavedNewsAdapter extends RecyclerView.Adapter<SavedNewsAdapter.ViewHolder> {

    private OnItemClickListener onItemClickListener;
    private Context context;
    private ArrayList<Article> savedItems;

    //List contains positions of saved Articles
    private ArrayList<Integer> articleListIndexes;


    //Constructor
    public SavedNewsAdapter(Context context) {
        this.context = context;
        savedItems = new ArrayList<>();
        articleListIndexes = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_news_feed, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView title = holder.title;
        TextView description = holder.description;
        TextView content = holder.content;
        ImageView feedImg = holder.feedImg;

        //Set item texts
        title.setText(savedItems.get(position).getTitle());
        description.setText(savedItems.get(position).getDescription());
        content.setText(savedItems.get(position).getContent());

        //Load and show an image of the article
        Glide.with(context).load(savedItems.get(position).getUrlToImage()).into(feedImg);
    }

    /*
     * Get size of saved list
     */
    @Override
    public int getItemCount() {
        return savedItems == null ? 0 : savedItems.size();
    }

    /*
     * Added item in saved articles list
     */
    public void saveItem(int articlePosition) {

        savedItems.add(DataControllers.getInstance().getNewsList().get(articlePosition));
        articleListIndexes.add(articlePosition);
        notifyDataSetChanged();
    }

    /*
     * Removed item from saved articles list
     */
    public void removeItem(int articlePosition) {

        DataControllers.getInstance().getNewsList().get(articlePosition).setStatus(false);
        int indexOfSaved = articleListIndexes.indexOf(articlePosition);
        savedItems.remove(indexOfSaved);
        articleListIndexes.remove(indexOfSaved);
        notifyDataSetChanged();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView description;
        TextView content;
        TextView title;

        ImageView feedImg;
        ImageView removeBtn;

        public ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            description = itemView.findViewById(R.id.description);
            content = itemView.findViewById(R.id.content);
            feedImg = itemView.findViewById(R.id.feedImg);
            removeBtn = itemView.findViewById(R.id.removeBtn);
            removeBtn.setVisibility(View.VISIBLE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(v, articleListIndexes.get(getAdapterPosition()));
                }
            });

            removeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeItem(articleListIndexes.get(getAdapterPosition()));
                }
            });

        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

}
