package com.testapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.testapp.R;
import com.testapp.controllers.DataControllers;
import com.testapp.interfaces.SavedCallBack;
import com.testapp.pojoobjects.Article;

/*
 * Detail of Article
 */
public class NewsDetailsFragment extends Fragment {

    private Article article;
    private int position;
    private boolean isSaved;

    private SavedCallBack savedCallBack;

    public NewsDetailsFragment() {
    }

    public static Fragment newInstance(int position) {

        Bundle args = new Bundle();

        NewsDetailsFragment fragment = new NewsDetailsFragment();
        fragment.setArguments(args);
        fragment.position = position;
        fragment.article = DataControllers.getInstance().getNewsList().get(position);
        fragment.isSaved = fragment.article.getStatus();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news_details, container, false);

        initToolBar(view);
        initViews(view);

        return view;
    }

    private void initViews(View view) {
        TextView title = view.findViewById(R.id.title);
        TextView description = view.findViewById(R.id.description);
        TextView content = view.findViewById(R.id.content);
        final View myLoader = view.findViewById(R.id.myLoader);
        final ImageView newsImage = view.findViewById(R.id.newsImage);
        final ImageView starSaved = view.findViewById(R.id.starSaved);

        starSaved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSaved) {
                    starSaved.setImageResource(R.drawable.ic_star_off);
                } else {
                    starSaved.setImageResource(R.drawable.ic_star_on);
                }
                isSaved = !isSaved;
                article.setStatus(isSaved);
                savedCallBack.saveItem(position, isSaved);
                DataControllers.getInstance().getNewsList().get(position).setStatus(isSaved);
            }
        });

        //Loaded image, after that showing star
        Glide.with(getActivity())
                .load(article.getUrlToImage())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target,
                                               boolean isFirstResource) {
                        myLoader.setVisibility(View.GONE);
                        if (isSaved) {
                            starSaved.setImageResource(R.drawable.ic_star_on);
                        } else {
                            starSaved.setImageResource(R.drawable.ic_star_off);
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model,
                                                   Target<GlideDrawable> target,
                                                   boolean isFromMemoryCache, boolean isFirstResource) {
                        myLoader.setVisibility(View.GONE);
                        if (isSaved) {
                            starSaved.setImageResource(R.drawable.ic_star_on);
                        } else {
                            starSaved.setImageResource(R.drawable.ic_star_off);
                        }
                        return false;
                    }
                })
                .into(newsImage);

        title.setText(article.getTitle());
        description.setText(article.getDescription());
        content.setText(article.getContent());

    }

    private void initToolBar(View view) {
        final TextView toolbarTitle = view.findViewById(R.id.toolbarTitle);
        toolbarTitle.setText(R.string.details);
        ImageView backBtn = view.findViewById(R.id.toolbarBackBtn);
        backBtn.setVisibility(View.VISIBLE);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }


    public void setSavedCallBack(SavedCallBack savedCallBack) {
        this.savedCallBack = savedCallBack;
    }

}
