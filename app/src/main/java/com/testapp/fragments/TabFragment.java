package com.testapp.fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.testapp.R;
import com.testapp.controllers.DataControllers;
import com.testapp.interfaces.TabCallBack;
import com.testapp.pojoobjects.Article;
import com.testapp.utils.Constants;
import com.testapp.utils.JsonParser;
import com.testapp.utils.PreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabFragment extends Fragment{
    public ArrayList<Fragment> fragments;

    public ViewPager viewPager;
    private NewsFeedAdapter pagerAdapter;
    private NewsFeedFragment newsFeedFragment;
    private SavedListFragment savedListFragment;

    public TabFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab, container, false);

        initToolBar(view);

        fragments = new ArrayList<>();

        pagerAdapter = new NewsFeedAdapter(this.getChildFragmentManager());
        viewPager = view.findViewById(R.id.container);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(2);
        TabLayout tabLayout = view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelected(true);
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
        tabLayout.setSelectedTabIndicatorHeight(10);
        setPageTitle(tabLayout);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        JSONObject o = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < DataControllers.getInstance().getNewsList().size(); i++) {

            JSONObject obj = DataControllers.getInstance().getNewsList().get(i).getJsonObject();
            jsonArray.put(obj);
        }
        try {
            o.put("articles", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PreferencesUtils.putString(getActivity(), Constants.PREFERENCES_TAG, o.toString());
    }

    private void initToolBar(View view) {
        final TextView toolbarTitle = view.findViewById(R.id.toolbarTitle);
        toolbarTitle.setText(R.string.app_name);
        view.findViewById(R.id.toolbarBackBtn).setVisibility(View.GONE);
    }

    public void setPageTitle(TabLayout tabLayout) {
        tabLayout.getTabAt(0).setText(R.string.news);
        tabLayout.getTabAt(1).setText(R.string.saved_news);
    }


    public class NewsFeedAdapter extends FragmentPagerAdapter implements TabCallBack {
        public NewsFeedAdapter(FragmentManager fm) {
            super(fm);
            addFragments();
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        public void addFragments() {

            newsFeedFragment = NewsFeedFragment.newInstance();
            newsFeedFragment.setTabCallBack(this);
            fragments.add(newsFeedFragment);

            savedListFragment = SavedListFragment.newInstance();
            fragments.add(savedListFragment);

        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public void tabCallBack(int position, boolean checked) {
            savedListFragment.saveItem(position, checked);
        }
    }


}
