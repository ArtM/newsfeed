package com.testapp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.testapp.R;
import com.testapp.activities.MainActivity;
import com.testapp.adapters.SavedNewsAdapter;
import com.testapp.controllers.DataControllers;
import com.testapp.interfaces.OnItemClickListener;
import com.testapp.interfaces.SavedCallBack;
import com.testapp.pojoobjects.Article;
import com.testapp.utils.Constants;
import com.testapp.utils.JsonParser;
import com.testapp.utils.PreferencesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class SavedListFragment extends Fragment implements SavedCallBack{

    private RecyclerView savedItemRv;
    private SavedNewsAdapter savedNewsAdapter;
    private View myLoader;

    public SavedListFragment() {
        // Required empty public constructor
    }


    public static SavedListFragment newInstance() {

        Bundle args = new Bundle();

        SavedListFragment fragment = new SavedListFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_saved_list, container, false);
        initViews(view);
        initSavedList();
        return view;
    }

    private void initViews(final View view) {

        savedItemRv = view.findViewById(R.id.savedItemRv);
        myLoader = view.findViewById(R.id.loader);

        savedNewsAdapter = new SavedNewsAdapter(getActivity());
        savedItemRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        savedItemRv.setAdapter(savedNewsAdapter);
        savedNewsAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View item, int position) {
                NewsDetailsFragment newsDetailsFragment = (NewsDetailsFragment) NewsDetailsFragment.newInstance(position);
                newsDetailsFragment.setSavedCallBack(SavedListFragment.this);
                ((MainActivity)getActivity()).pushFragment(newsDetailsFragment, true);
            }
        });

        myLoader.setVisibility(View.GONE);
    }

    /*
     * Initialize Saved list
     * get data from preferences
     */
    private void initSavedList(){
        try {
            JSONObject obj = new JSONObject(PreferencesUtils.getString(getActivity(), Constants.PREFERENCES_TAG, ""));
            ArrayList<Article> data = JsonParser.parsNewsList(obj.getJSONArray("articles"));
            DataControllers.getInstance().setNewsList(data);
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getStatus()){
                    savedNewsAdapter.saveItem(i);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /*
     * If saved star checked, item will be added
     * Else item will be removed
     */
    @Override
    public void saveItem(int position, boolean checked) {
        if (savedNewsAdapter == null){
            savedNewsAdapter = new SavedNewsAdapter(getActivity());
        }
        if (checked) {
            savedNewsAdapter.saveItem(position);
        }else {
            savedNewsAdapter.removeItem(position);
        }
    }
}
