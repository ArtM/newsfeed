package com.testapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.testapp.R;
import com.testapp.activities.MainActivity;
import com.testapp.adapters.ArticleAdapter;
import com.testapp.controllers.DataControllers;
import com.testapp.interfaces.OnItemClickListener;
import com.testapp.interfaces.SavedCallBack;
import com.testapp.interfaces.TabCallBack;
import com.testapp.networkcontroller.WebServiceClient;
import com.testapp.pojoobjects.Article;
import com.testapp.utils.Constants;
import com.testapp.utils.JsonParser;
import com.testapp.utils.PreferencesUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;


public class NewsFeedFragment extends Fragment implements SavedCallBack {

    private RecyclerView newsRv;
    private ArticleAdapter articleAdapter;

    private RelativeLayout coordinatorLayout;

    private SwipeRefreshLayout swipeRefreshLayout;

    private SavedListFragment savedListFragment;

    private TabCallBack tabCallBack;

    private View myLoader;

    public NewsFeedFragment() {
        // Required empty public constructor
    }


    public static NewsFeedFragment newInstance() {

        Bundle args = new Bundle();

        NewsFeedFragment fragment = new NewsFeedFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_news_feed, container, false);
        initViews(view);

        return view;

    }

    private void initViews(final View view) {

        newsRv = view.findViewById(R.id.newsRv);

        myLoader = view.findViewById(R.id.loader);
        coordinatorLayout = view.findViewById(R.id.coordinatorLayout);

        articleAdapter = new ArticleAdapter(getActivity());

        newsRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        newsRv.setAdapter(articleAdapter);

        savedListFragment = SavedListFragment.newInstance();

        articleAdapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View item, int position) {
                NewsDetailsFragment f = (NewsDetailsFragment) NewsDetailsFragment.newInstance(position);
                f.setSavedCallBack(NewsFeedFragment.this);
                ((MainActivity) getActivity()).pushFragment(f, true);
            }
        });

        setSwipeRefresh(view);

        //Get the preferences result
        String preferencesNews = PreferencesUtils.getString(getActivity(), Constants.PREFERENCES_TAG, null);
        if (preferencesNews!=null){
            try {
                JSONObject obj = new JSONObject(preferencesNews);
                ArrayList<Article> data = JsonParser.parsNewsList(obj.getJSONArray("articles"));
                articleAdapter.addNewsList(data);
                DataControllers.getInstance().setNewsList(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else {
            sendNewsFeedRequest();
        }
        setSwipeRefresh(view);

    }

    private void setSwipeRefresh(final View view) {
        swipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (((MainActivity) getActivity()).isNetworkConnected()) {
                    sendNewsFeedRequest();
                } else {
                    Snackbar snackbar = Snackbar
                            .make(view, R.string.no_internet, Snackbar.LENGTH_LONG);

                    snackbar.show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    /*
     * Send NewsFeed Request
     */
    private void sendNewsFeedRequest() {

        if (((MainActivity) getActivity()).isNetworkConnected()) {

            myLoader.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setRefreshing(false);


            RequestParams params = new RequestParams();
            params.put("q", "bitcoin");
            params.put("from", getCurrentDate());
            params.put("sortBy", "publishedAt");
            params.put("apiKey", getActivity().getResources().getString(R.string.api_key));

            WebServiceClient.getNews(getActivity(), params, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    super.onSuccess(statusCode, headers, response);
                    swipeRefreshLayout.setRefreshing(false);
                    myLoader.setVisibility(View.GONE);
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    super.onSuccess(statusCode, headers, response);

                    ArrayList<Article> data = null;
                    try {
                        data = JsonParser.parsNewsList(response.getJSONArray("articles"));
                        PreferencesUtils.putString(getActivity(), Constants.PREFERENCES_TAG, response.toString());
                        articleAdapter.addNewsList(data);
                        DataControllers.getInstance().setNewsList(data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    myLoader.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    if (!((MainActivity) getActivity()).isNetworkConnected()) {
                        Snackbar snackbar = Snackbar
                                .make(coordinatorLayout, R.string.no_internet, Snackbar.LENGTH_LONG);

                        snackbar.show();
                    } else {
                        Toast.makeText(getActivity(), R.string.please_update_page_again, Toast.LENGTH_SHORT).show();
                    }
                    myLoader.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    myLoader.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    myLoader.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                }

            });

        }
    }

    private String getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = format.format(calendar.getTime());

        return strDate;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!((MainActivity) getActivity()).isNetworkConnected()) {
            Snackbar snackbar = Snackbar
                    .make(view, R.string.no_internet, Snackbar.LENGTH_LONG);

            snackbar.show();
        }
    }

    @Override
    public void saveItem(int position, boolean checked) {
        savedListFragment.saveItem(position, checked);
        tabCallBack.tabCallBack(position, checked);
    }

    public void setTabCallBack(TabCallBack tabCallBack) {
        this.tabCallBack = tabCallBack;
    }
}
