package com.testapp.utils;


import android.content.Context;
import android.content.SharedPreferences;

public class PreferencesUtils {

    public static void putString(Context context, String key, String value) {
        SharedPreferences sPref = context.getSharedPreferences(Constants.PREFERENCES_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sPref.edit();
        ed.putString(key, value);
        ed.apply();
    }

    public static String getString(Context context, String key, String defValue) {
        SharedPreferences sPref = context.getSharedPreferences(Constants.PREFERENCES_TAG, Context.MODE_PRIVATE);
        return sPref.getString(key, defValue);

    }

}
