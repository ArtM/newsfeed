package com.testapp.utils;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.testapp.pojoobjects.Article;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonParser {
    public static ArrayList<Article> parsNewsList(JSONArray jsonArray) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            if (jsonArray != null) {
                    return mapper.readValue(jsonArray.toString(), new TypeReference<List<Article>>() {
                    });
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

